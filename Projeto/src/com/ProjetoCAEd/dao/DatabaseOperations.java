package com.ProjetoCAEd.dao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ProjetoCAEd.pojo.Estudante;
import com.ProjetoCAEd.util.HibernateUtil;

public class DatabaseOperations {

	private static Transaction transObj;
	private static Session sessionObj = HibernateUtil.getSessionFactory().openSession();

	// Metodo para adicionar um novo estudante na base de dados
	public void addEstudanteInDb(Estudante estudanteObj) {		
		try {
			transObj = sessionObj.beginTransaction();
			sessionObj.save(estudanteObj);
			System.out.println("Estudante com o Id: " + estudanteObj.getId() + " cadastrado com sucesso.");

			// Retorno XHTML
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("createdEstudanteId",  estudanteObj.getId());						
		} catch (Exception exceptionObj) {
			exceptionObj.printStackTrace();
		} finally {
			transObj.commit();
		}
	}

	// Metodo para excluir um estudante da base de dados
	public void deleteEstudanteInDb(int delEstudanteId) {
		try {
			transObj = sessionObj.beginTransaction();
			Estudante estudId = (Estudante)sessionObj.load(Estudante.class, new Integer(delEstudanteId));
			sessionObj.delete(estudId);
			System.out.println("Registro do estudante com Id: " + delEstudanteId + " removido com sucesso.");

			// Retorno XHTML
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("deletedEstudanteId",  delEstudanteId);	
		} catch (Exception exceptionObj) {
			exceptionObj.printStackTrace();
		} finally {
			transObj.commit();
		}
	}

	// Metodo para pesquisar um estudante na base de dados
	public List<Estudante> getEstudanteById(int estudanteId) {	
		Estudante particularEstuDObj = new Estudante();
		List<Estudante> particularEstudanteList = new ArrayList<Estudante>();            
		try {
			transObj = sessionObj.beginTransaction();
			Query queryObj = sessionObj.createQuery("from Estudante where id= :estudante_id").setInteger("estudante_id", estudanteId);			
			particularEstuDObj = (Estudante)queryObj.uniqueResult();
			particularEstudanteList = queryObj.list();			
			System.out.println("Exibindo o estudante com o Id: " + estudanteId + "");

			// Retorno XHTML
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("findEstudanteById",  estudanteId);
		} catch(Exception exceptionObj) {
			exceptionObj.printStackTrace();
		} finally {
			transObj.commit();
		}
		return particularEstudanteList;
	}

	// Metodo para atualizar o registro de um estudante na base de dados	
	public void updateEstudanteRegistro(Estudante updateEstudanteObj) {
		try {
			transObj = sessionObj.beginTransaction();
			sessionObj.update(updateEstudanteObj);		
			System.out.println("Registro do estudante com o Id: " + updateEstudanteObj.getId() + " atualizado com sucesso.");	

			// Retorno XHTML
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("updatedEstudanteRegistro",  "Success");
		} catch(Exception exceptionObj){
			exceptionObj.printStackTrace();
		} finally {
			transObj.commit();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Estudante> retrieveEstudante() {		
		Estudante estudantesObj;
		List<Estudante>allEstudantes = new ArrayList<Estudante>();
		try {
			transObj = sessionObj.beginTransaction();
			Query queryObj = sessionObj.createQuery("from Estudante");
			allEstudantes = queryObj.list();
			for(Estudante estud : allEstudantes) {
				estudantesObj = new Estudante(); 								
				estudantesObj.setNome(estud.getNome());
				estudantesObj.setCurso(estud.getCurso());								
				allEstudantes.add(estudantesObj);  
			}			
			System.out.println("Exibindo todos os registros de estudantes.");

			// Retorno XHTML
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("findEstudanteById", "true");
		} catch(Exception exceptionObj) {
			exceptionObj.printStackTrace();
		} finally {
			transObj.commit();
		}
		return allEstudantes;
	}
}