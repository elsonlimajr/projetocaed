/* Cria��o da base */
CREATE DATABASE IF NOT EXISTS ProjetoCAEd;

/* Setando a utiliza��o da base */
USE ProjetoCAEd;

/* DROP de qualquer tabela com o nome "Estudante" */
DROP TABLE IF EXISTS estudante;

/* Criando a tabela estudante na base ProjetoCAEd*/
CREATE TABLE estudante (
	id int NOT NULL,
	Nome VARCHAR(100) NOT NULL,
	Curso VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;