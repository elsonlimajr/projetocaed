package com.jsfhibernate.pojo;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.jsfhibernate.dao.DatabaseOperations;

@ManagedBean @SessionScoped
public class Estudante implements java.io.Serializable {

	private int id;
	private String nome;
	private String curso;
	private List<Estudante> estudanteList;	
	public static DatabaseOperations dbObj;
	private static final long serialVersionUID = 1L;

	public Estudante() { }

	public Estudante(int id) {
		this.id = id;
	}

	public Estudante(int id, String nome, String curso) {
		this.id = id;
		this.nome = nome;
		this.curso = curso;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCurso() {
		return this.curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public List<Estudante> getEstudanteList() {
		return estudanteList;
	}

	public void setEstudanteList(List<Estudante> estudanteList) {
		this.estudanteList = estudanteList;
	}

	// Metodo para adicionar um novo registro na tabela de estudante
	public void saveEstudanteRegistro() {
		dbObj = new DatabaseOperations();
		dbObj.addEstudanteInDb(this);
	}

	// Metodo para deletar um estudante da base de dados
	public void deleteEstudanteRegistro() {
		dbObj = new DatabaseOperations();
		dbObj.deleteEstudanteInDb(id);
	}

	// Metodo para buscar um estudante a partir do id na base 
	public List<Estudante> getEstudanteDetalhesById() {
		dbObj = new DatabaseOperations();		
		estudanteList = dbObj.getEstudanteById(id);
		for(Estudante selectedEstud : estudanteList) {
			nome = selectedEstud.getNome();
			curso = selectedEstud.getCurso();
		}
		System.out.println("Encontrado Id? " + id + " informações sobre o estudante: Nome=" + nome + ", Curso=" + curso);
		return estudanteList;
	}

	// Metodo para atualizar os dados de um estudante
	public void updateEstudanteDetalhes() {
		dbObj = new DatabaseOperations();		
		dbObj.updateEstudanteRegistro(this);
	}

	// Metodo para listar todos os estudantes cadastrados
	public List<Estudante> getAllEstudanteRegistros() {
		dbObj = new DatabaseOperations();		
		List<Estudante> estudantesList = dbObj.retrieveEstudante();
		for(Estudante estud : estudantesList) {
			if(estud.getId() == id) {
				nome = estud.getNome();
				curso = estud.getCurso();
			}
		}
		return estudantesList;
	}
}